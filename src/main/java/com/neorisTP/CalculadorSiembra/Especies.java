package com.neorisTP.CalculadorSiembra;

public class Especies {
 private String especie;
 private float distanciamiento;
 private int[] fecha;
 private int diasCosecha;
 
 
  public Especies(String especie, float distanciamiento, int[] fecha, int diasCosecha) {
	  this.especie = especie;
	  this.distanciamiento = distanciamiento;
	  this.fecha = fecha;
	  this.diasCosecha = diasCosecha;
  }


public String getEspecie() {
	return especie;
}


public void setEspecie(String especie) {
	this.especie = especie;
}


public float getDistanciamiento() {
	return distanciamiento;
}


public void setDistanciamiento(float distanciamiento) {
	this.distanciamiento = distanciamiento;
}

public int[] getFecha() {
	return fecha;
}


public void setFecha(int[] fecha) {
	this.fecha = fecha;
}


public int getDiasCosecha() {
	return diasCosecha;
}


public void setDiasCosecha(int diasCosecha) {
	this.diasCosecha = diasCosecha;
}
	
}
