package com.neorisTP.CalculadorSiembra;

import java.util.ArrayList;
import java.util.List;
import java.time.*;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@CrossOrigin
@RestController
public class CalculadorSiembraApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculadorSiembraApplication.class, args);
	}

	private List<Especies> listaEspecies = new ArrayList<>();

	public List<Especies> armoListaEspecies() {

		int[] mesesAcelga = { 2, 3, 4, 5, 6, 8, 9, 10, 11, 12 };
		int[] mesesAjo = { 3, 4, 5 };
		int[] mesesApio = { 1, 2, 3, 9, 10, 11, 12 };
		int[] mesesEsparrago = { 8, 9 };
		int[] mesesEspinaca = { 2, 3, 4, 5, 6 };
		int[] mesesLechuga = { 8, 9, 10, 11, 12 };
		int[] mesesPapa = { 1, 2, 8, 9 };
		int[] mesesPimiento = { 9 };
		int[] mesesRabanito = { 2, 3, 4, 5, 6, 9, 10, 11, 12 };
		int[] mesesRemolacha = { 3, 4, 5, 6, 8, 9, 10, 11, 12 };

		listaEspecies.add(new Especies("Acelga", 0.2f, mesesAcelga, 80));
		listaEspecies.add(new Especies("Ajo", 0.15f, mesesAjo, 270));
		listaEspecies.add(new Especies("Apio", 0.25f, mesesApio, 150));
		listaEspecies.add(new Especies("Esparragos", 0.30f, mesesEsparrago, 1825));
		listaEspecies.add(new Especies("Espinacas", 0.1f, mesesEspinaca, 90));
		listaEspecies.add(new Especies("Lechuga", 0.2f, mesesLechuga, 90));
		listaEspecies.add(new Especies("Papa", 0.3f, mesesPapa, 150));
		listaEspecies.add(new Especies("Pimiento", 0.45f, mesesPimiento, 200));
		listaEspecies.add(new Especies("Rabanito", 0.05f, mesesRabanito, 40));
		listaEspecies.add(new Especies("Remolacha", 0.1f, mesesRemolacha, 130));

		return listaEspecies;
	}

	@GetMapping("/saludo")
	public Especies saludo() {
		Especies pepe = listaEspecies.get(0);

		return pepe;
	}

	@GetMapping("/getEspecie/{especieRecibida}")
	public String getEspecie(@PathVariable("especieRecibida") String especieRecibida,
			@RequestParam(value = "largo", defaultValue = "0") String largo,
			@RequestParam(value = "ancho", defaultValue = "0") String ancho) {
		float miLargo = Float.parseFloat(largo);
		float miAncho = Float.parseFloat(ancho);

		int numero = 100;
		if (especieRecibida.equalsIgnoreCase("acelga")) {
			numero = 0;
		} else if (especieRecibida.equalsIgnoreCase("ajo")) {
			numero = 1;
		} else if (especieRecibida.equalsIgnoreCase("apio")) {
			numero = 2;
		} else if (especieRecibida.equalsIgnoreCase("esparrago")) {
			numero = 3;
		} else if (especieRecibida.equalsIgnoreCase("espinaca")) {
			numero = 4;
		} else if (especieRecibida.equalsIgnoreCase("lechuga")) {
			numero = 5;
		} else if (especieRecibida.equalsIgnoreCase("papa")) {
			numero = 6;
		} else if (especieRecibida.equalsIgnoreCase("pimiento")) {
			numero = 7;
		} else if (especieRecibida.equalsIgnoreCase("rabanito")) {
			numero = 8;
		} else if (especieRecibida.equalsIgnoreCase("remolacha")) {
			numero = 9;
		} else {
			numero = 100;
		}
		String especie;
		String salida = "";
		float distancia = 0f;
		float cantPlantines = 0;
		LocalDate fechaHoy = LocalDate.now();
		LocalDate fechaCosecha;
		String salidaFechas;
		int mesActual = LocalDate.now().getMonthValue();
		int[] mesesCosecha;
		boolean sePuedeSembrarHoy = false;

		if (miLargo <= 0 || miAncho <= 0) {
			salida = "Los datos ingresados son incorrectos";
		} else {

			if (numero != 100) {
				especie = armoListaEspecies().get(numero).getEspecie();
				distancia = armoListaEspecies().get(numero).getDistanciamiento();
				cantPlantines = (miLargo / distancia) * (miAncho / distancia);
				mesesCosecha = armoListaEspecies().get(numero).getFecha();

				for (int i = 0; i < mesesCosecha.length; i++) {
					if(mesActual == mesesCosecha[i]) {
						sePuedeSembrarHoy = true;						
					}
				}


				if (sePuedeSembrarHoy) {
					fechaCosecha = fechaHoy.plusDays(armoListaEspecies().get(numero).getDiasCosecha());
					salidaFechas = ". La fecha estimada de cosecha será " + fechaCosecha;
					salida = "En " + miLargo + " mts de largo por " + miAncho + " mts de ancho, se podrá plantar "
							+ Math.round(cantPlantines) + " unidades de plantines de " + especie + salidaFechas;
				} else {
					salida = "La especie consultada está fuera de calendario";
				}

			} else {
				salida = "No se encontraron datos de dicha especie";
			}

		}
		return salida;

	}

}
